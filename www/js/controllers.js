var controllers = angular.module('starter.controllers', ['starter.services'])

.controller('AppCtrl', function($scope, $rootScope, $ionicModal, $timeout, $auth) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  $rootScope.login = $scope.login;

  $scope.logout = function() {
    $timeout(function() {
      $auth.signOut()
        .then(function(resp) {
          alert("User successfully signed out.");
        })
        .catch(function(resp){
          alert("There was server problem, try again.");
        });
    },0);
  };
  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login with:', $scope.loginData);
    $timeout(function() {
      $auth.submitLogin({
        email: $scope.loginData.email,
        password: $scope.loginData.password
      })
        .then(function(resp) {
          alert("User successfully signed in.");
          $scope.modal.hide();
          //console.log($auth.retrieveData('auth_headers'));
          //console.log($auth.getExpiry());
        })
        .catch(function(resp) {
          alert(resp.errors);
          $scope.login();
        });
    },0);
  };
});