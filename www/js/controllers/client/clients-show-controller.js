controllers.controller('ClientsShowCtrl', ['$scope', '$stateParams','$location', 'Client', 'Order', 
  function($scope, $stateParams, $location, Client, Order) {
  //$scope.categories = Category.query();
  Client.show($stateParams.clientId)
  .success(function(data) { 
    $scope.client = data.client;
  });

  $scope.addOrder = function() {
    Order.create_article($scope.client.id).success(function(data) {
      $location.path("/app/orders/" + data.order.id);
      console.log($location.path());
    });
  };
}]);