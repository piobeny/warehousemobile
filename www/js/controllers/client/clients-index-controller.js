controllers.controller('ClientsIndexCtrl', function($scope, Client) {
  Client.all()
  .success(function(data) { 
    $scope.clients = data.clients; 
  });
});