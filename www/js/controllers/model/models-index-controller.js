controllers.controller('ModelsIndexCtrl', function($scope, Model) {
  Model.all()
  .success(function(data) { 
    $scope.models = data.models; 
  });
});