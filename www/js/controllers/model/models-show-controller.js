controllers.controller('ModelsShowCtrl', ['$scope', '$stateParams', 'Model', function($scope, $stateParams, Model) {
  //$scope.categories = Category.query();
  Model.show($stateParams.modelId)
  .success(function(data) { 
    $scope.model = data.model;
  });
}]);