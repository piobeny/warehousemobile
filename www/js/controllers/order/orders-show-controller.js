controllers.controller('OrdersShowCtrl', ['$scope', '$stateParams', '$ionicModal', '$ionicPopup', 'Order', 'Article', 
    function($scope, $stateParams, $ionicModal, $ionicPopup, Order, Article) {
  //$scope.categories = Category.query();
  Order.show($stateParams.orderId)
  .success(function(data) { 
    $scope.order = data.order;
    $scope.order_articles = data.order_articles;
  });


  $scope.order_article = {};

  $ionicModal.fromTemplateUrl('templates/orders/form_choice.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.formModal = modal;
  });

  $scope.addArticle = function() {
    Article.all($stateParams.modelId)
      .success(function(data) { 
        $scope.articles = data.articles; 
      });
    $scope.formModal.show();
  };

  $scope.closeForm = function() {
    $scope.formModal.hide();
  };

  $scope.insertQuantity = function(article) {
    $scope.tempPrice = article.price;
    $scope.maxQuantity = article.quantity;
    $scope.order_article.article_id = article.id;
    var quantityPopup = $ionicPopup.show({
      templateUrl: 'templates/orders/form_input_quantity.html',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Add</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.order_article.quantity) {

              e.preventDefault();
            } else {
              //return $scope.order_article;
              Order.add_article($stateParams.orderId, $scope.order_article)
                .success(function(data) { 
                  $scope.closeForm();

                  $scope.order_articles.push(data.order_article);
                  $scope.order.summary_price = parseFloat($scope.order.summary_price) 
                    + data.order_article.price * data.order_article.quantity;
                  $scope.order_article = {};
                });
            }
          }
        }
      ]
    });
  };

  $scope.quantityFilter = function(article) {
    return ( article.quantity > 0 );
  };

  $scope.deleteArticle = function(order_article_id, article_name, array_id) {
    //alert(order_article_id);
    var res = $scope.showConfirm(order_article_id, article_name, array_id);
    //
  };

  $scope.showConfirm = function(order_article_id, name, array_id) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Are you sure to delete article?',
      template: name
    });
    confirmPopup.then(function(res) {
    if(res) {
      Order.remove_article($stateParams.orderId, order_article_id).success(function() {
        //alert(array_id)
        var article = $scope.order_articles[array_id];
        $scope.order_articles.splice(array_id, 1);
        $scope.order.summary_price = parseFloat($scope.order.summary_price) - article.price * article.quantity;
      });
    } else {
      return false;
    }
    });
  };
}]);