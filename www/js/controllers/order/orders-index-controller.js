controllers.controller('OrdersIndexCtrl', ['$scope', '$stateParams', 'Order', function($scope, $stateParams, Order) {

  Order.all($stateParams.clientId)
  .success(function(data) { 
    $scope.orders = data.clients;
  });
}]);