controllers.controller('ManufacturesShowCtrl', ['$scope', '$stateParams', 'Manufacture', function($scope, $stateParams, Manufacture) {
  //$scope.categories = Category.query();
  Manufacture.show($stateParams.manufactureId)
  .success(function(data) { 
    $scope.manufacture = data.manufacture;
    $scope.models = data.models;
    $scope.modelQuantity = 5;
  });
}]);