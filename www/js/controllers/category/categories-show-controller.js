controllers.controller('CategoriesShowCtrl', ['$scope', '$stateParams', 'Category', function($scope, $stateParams, Category) {
  //$scope.categories = Category.query();
  Category.show($stateParams.categoryId)
  .success(function(data) { 
    $scope.category = data.category;
    $scope.models = data.models;
    $scope.modelQuantity = 5;
  });
}]);