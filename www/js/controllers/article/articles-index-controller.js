controllers.controller('ArticlesIndexCtrl', ['$scope', '$stateParams', 'Article', function($scope, $stateParams, Article) {

  Article.all($stateParams.modelId)
  .success(function(data) { 
    $scope.articles = data.models; 
  });
}]);