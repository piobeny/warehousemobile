controllers.controller('ArticlesShowCtrl', ['$scope', '$stateParams', 'Article', function($scope, $stateParams, Article) {
  
  Article.show($stateParams.articleId)
  .success(function(data) { 
    $scope.article = data.article;
  });
}]);