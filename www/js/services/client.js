services.factory("Client", ['$http', '$resource', '$auth','config', function ($http, $resource, $auth, config) {
  
  //var url = config.apiUrl + '/clients'
  var url = $auth.apiUrl() + '/clients';
  return {
    all: function() { 
      return $http({method: 'GET', url: url + '.json' });
      //return $http.get(url + '.json', { headers: $auth.retrieveData('auth_headers') });
    },

    show: function(id) {
      return $http({method: 'GET', url: url + '/' + id + '.json' })
      //console.log($auth.retrieveData('auth_headers'));
      //return $http.get(url + '/' + id + '.json', { headers: $auth.retrieveData('auth_headers') });
      //headers: 
    }
  }; 
}]);