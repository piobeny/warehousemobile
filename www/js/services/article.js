services.factory("Article", ['$http', '$resource', 'config', function ($http, $resource, config) {
  //return $resource(config.apiUrl + '/categories.json');
  var url = config.apiUrl + '/articles'
  return {
    all: function(model_id) {
      if (model_id) {
        return $http({method: 'GET', url: config.apiUrl + "/models/" + model_id + '/articles.json' });
      }
      else {
        return $http({method: 'GET', url: url + '.json' });
      }
    },

    show: function(id) {
      return $http({method: 'GET', url: url + '/' + id + '.json' })
    }
  }; 
}]);