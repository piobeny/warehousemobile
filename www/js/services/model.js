services.factory("Model", ['$http', '$resource', 'config', function ($http, $resource, config) {
  //return $resource(config.apiUrl + '/categories.json');
  var url = config.apiUrl + '/models'
  return {
    all: function() { 
      return $http({method: 'GET', url: url + '.json' });
      //return $resource(config.apiUrl + '/categories.json'); 
    },

    show: function(id) {
      return $http({method: 'GET', url: url + '/' + id + '.json' })
    }
  }; 
}]);