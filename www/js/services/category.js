var services = angular.module("starter.services", ['ngResource'])
.factory("Category", ['$http', '$resource', 'config', function ($http, $resource, config) {//CategoryFactory($resource) {
  //return $resource(config.apiUrl + '/categories.json');
  var url = config.apiUrl + '/categories'
  return {
    all: function() { 
      return $http({method: 'GET', url: url + '.json' });
      //return $resource(config.apiUrl + '/categories.json'); 
    },

    show: function(id) {
      return $http({method: 'GET', url: url + '/' + id + '.json' })
    }
  }; 
}]);