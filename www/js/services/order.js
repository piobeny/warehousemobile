services.factory("Order", ['$http', '$resource', 'config', function ($http, $resource, config) {

  var url = config.apiUrl + '/orders'
  return {
    all: function(client_id) { 
      return $http({method: 'GET', url: config.apiUrl + '/clients/' + client_id + '/orders.json' });
    },

    show: function(id) {
      return $http({method: 'GET', url: url + '/' + id + '.json' })
    },

    add_article: function(id, order_article) {
      return $http.post(url + '/' + id + '/add_article.json', order_article);
    },

    remove_article: function(id, order_article_id) {
      return $http.post(url + '/' + id + '/remove_article.json', {order_article_id: order_article_id});
    },

    create_article: function(client_id) {
      return $http.post(url + '.json', { client_id: client_id })
    }
  }; 
}]);