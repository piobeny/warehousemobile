// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', ['ionic', 'starter.controllers', 'ng-token-auth'])

.run(function($ionicPlatform, $rootScope, $location) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

  $rootScope.$on('auth:login-success', function() {
    $location.path('/app/clients');
  });
  $rootScope.$on('auth:invalid', function() {
    $rootScope.login();
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

  $httpProvider.defaults.useXDomain = true; 
  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  var res = { auth: ['$auth', function($auth) { 
    return $auth.validateUser();
  }] };

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.categories', {
    url: '/categories',
    views: {
      'menuContent': {
        templateUrl: 'templates/categories/index.html',
        controller: 'CategoriesIndexCtrl',
        resolve: res
      }
    }
  })

  .state('app.category', {
    url: '/categories/:categoryId',
    views: {
      'menuContent': {
        templateUrl: 'templates/categories/show.html',
        controller: 'CategoriesShowCtrl',
        resolve: res
      }
    }
  })

  .state('app.clients', {
    url: '/clients',
    views: {
      'menuContent': {
        templateUrl: 'templates/clients/index.html',
        controller: 'ClientsIndexCtrl',
        resolve: res
      }
    }
  })

  .state('app.client', {
    url: '/clients/:clientId',
    views: {
      'menuContent': {
        templateUrl: 'templates/clients/show.html',
        controller: 'ClientsShowCtrl',
        resolve: res
      }
    }
  })

  .state('app.order', {
    url: '/orders/:orderId',
    views: {
      'menuContent': {
        templateUrl: 'templates/orders/show.html',
        controller: 'OrdersShowCtrl',
        resolve: res
      }
    }
  })

  .state('app.orders', {
    url: '/clients/:clientId/orders',
    views: {
      'menuContent': {
        templateUrl: 'templates/orders/index.html',
        controller: 'OrdersIndexCtrl',
        resolve: res
      }
    }
  })

  .state('app.manufactures', {
    url: '/manufactures',
    views: {
      'menuContent': {
        templateUrl: 'templates/manufactures/index.html',
        controller: 'ManufacturesIndexCtrl',
        resolve: res
      }
    }
  })

  .state('app.manufacture', {
    url: '/manufactures/:manufactureId',
    views: {
      'menuContent': {
        templateUrl: 'templates/manufactures/show.html',
        controller: 'ManufacturesShowCtrl',
        resolve: res
      }
    }
  })

  .state('app.models', {
    url: '/models',
    views: {
      'menuContent': {
        templateUrl: 'templates/models/index.html',
        controller: 'ModelsIndexCtrl',
        resolve: res
      }
    }
  })

  .state('app.model', {
    url: '/models/:modelId',
    views: {
      'menuContent': {
        templateUrl: 'templates/models/show.html',
        controller: 'ModelsShowCtrl',
        resolve: res
      }
    }
  })
  .state('app.articles', {
    url: '/models/:modelId/articles',
    views: {
      'menuContent': {
        templateUrl: 'templates/articles/index.html',
        controller: 'ArticlesIndexCtrl',
        resolve: res
      }
    }
  })

  .state('app.article', {
    url: '/articles/:articleId',
    views: {
      'menuContent': {
        templateUrl: 'templates/articles/show.html',
        controller: 'ArticlesShowCtrl',
        resolve: res
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  //$urlRouterProvider.otherwise('/app/search');
  $urlRouterProvider.otherwise('/app/search');
})

.directive('searchList', function(){
  return {
    restrict: 'E',
    templateUrl: 'templates/shared/search-list.html'
  };
})

.constant('config', {
    //apiUrl: 'http://192.168.0.10/api'
    apiUrl: '/api'
})

.config(function($authProvider) {
    $authProvider.configure({
        apiUrl: '/api'
        //apiUrl: 'http://192.168.0.10/api'
    });
})
.filter('defaultDate', function($filter) {    
    var angularDateFilter = $filter('date');
    return function(theDate) {
       return angularDateFilter(theDate, 'dd MMMM @ HH:mm:ss');
    }
});